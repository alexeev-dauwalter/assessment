FROM node:lts AS build
WORKDIR /app
COPY . /app
RUN npm i
RUN npm run build

FROM nginx:1.24
COPY --from=build /app/dist /usr/share/nginx/html
EXPOSE 80
